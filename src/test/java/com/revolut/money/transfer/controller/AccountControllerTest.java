package com.revolut.money.transfer.controller;

import com.revolut.money.transfer.config.ServerConfig;
import io.javalin.Javalin;
import io.restassured.http.ContentType;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import util.TestJavalinServer;

import java.io.File;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@RunWith(JUnit4.class)
public class AccountControllerTest {

    private static final int PORT = 8182;

    private static final String URL = String.format("http://localhost:%d/accounts", PORT);

    private static final String REQUEST_CREATE_ACCOUNT_JSON = "src/test/resources/account/request/create_account.json";

    private static final String REQUEST_UPDATE_ACCOUNT_JSON = "src/test/resources/account/request/update_account.json";

    private static final String IBAN = "iban";

    private static final String LAST_NAME = "lastName";

    private static final String AMOUNT = "amount";

    private static final String PHONE = "phone";

    private static final String EMAIL = "email";

    private static final String PATH_PARAM_IBAN = "/{iban}";

    private static final String FIRST_NAME = "firstName";

    private static Javalin server;

    @BeforeClass
    public static void setUp() {
        server = TestJavalinServer.createServer().start(PORT);
    }

    @AfterClass
    public static void tearDown() {
        server.stop();
    }

    @Before
    public void init() {
        ServerConfig.initializeDB();
    }

    @After
    public void cleanUp() {
        ServerConfig.cleanUpDB();
    }

    @Test
    public void test_givenAccounts_whenGetAll_returnCorrectNumberOfAccounts() {
        given()
                .contentType(ContentType.JSON)
                .when()
                .get(URL)
                .then()
                .assertThat()
                .statusCode(200)
                .and()
                .body("size()", equalTo(4));
    }

    @Test
    public void test_givenAccounts_whenGetOne_returnCorrectAccount() {
        given()
                .contentType(ContentType.JSON)
                .pathParam(IBAN, "DE89370400440532013000")
                .when()
                .get(URL + PATH_PARAM_IBAN)
                .then()
                .assertThat()
                .statusCode(200)
                .and()
                .body(IBAN, equalTo("DE89370400440532013000"),
                        FIRST_NAME, equalTo("Kerstin"),
                        LAST_NAME, equalTo("Goldschmidt"),
                        AMOUNT, equalTo(1578.34f),
                        PHONE, equalTo("0490421868174"),
                        EMAIL, equalTo("KerstinGoldschmidt@dayrep.com"));
    }

    @Test
    public void test_givenAccount_whenCreate_returnCreatedAccount() {
        given()
                .contentType(ContentType.JSON)
                .body(new File(REQUEST_CREATE_ACCOUNT_JSON))
                .when()
                .post(URL)
                .then()
                .assertThat()
                .statusCode(201)
                .and()
                .body(IBAN, equalTo("TEST1234567890"),
                        FIRST_NAME, equalTo("TestUser"),
                        LAST_NAME, equalTo("TestUser"),
                        AMOUNT, equalTo(1500.57f),
                        PHONE, equalTo("123456789"),
                        EMAIL, equalTo("testuser@email.com"));
    }

    @Test
    public void test_givenAccount_whenDelete_returnAccountDeleted() {
        given()
                .contentType(ContentType.JSON)
                .pathParam(IBAN, "DE89370400440532013000")
                .when()
                .delete(URL + PATH_PARAM_IBAN)
                .then()
                .assertThat()
                .statusCode(202);
    }

    @Test
    public void test_givenAccount_whenUpdate_returnUpdatedAccount() throws InterruptedException {
        given()
                .contentType(ContentType.JSON)
                .pathParam(IBAN, "SE3550000000054910000003")
                .body(new File(REQUEST_UPDATE_ACCOUNT_JSON))
                .when()
                .patch(URL + PATH_PARAM_IBAN)
                .then()
                .assertThat()
                .statusCode(204);
    }
}