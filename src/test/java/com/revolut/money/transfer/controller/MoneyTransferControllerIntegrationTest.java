package com.revolut.money.transfer.controller;

import com.revolut.money.transfer.config.ServerConfig;
import com.revolut.money.transfer.service.Status;
import io.javalin.Javalin;
import io.restassured.http.ContentType;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import util.TestJavalinServer;

import java.io.File;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@RunWith(JUnit4.class)
public class MoneyTransferControllerIntegrationTest {

    private static final int PORT = 8181;

    private static final String URL = String.format("http://localhost:%d/accounts", PORT);

    private static final String REQUEST_TRANSFER_MONEY_JSON = "src/test/resources/transfer/request/transfer_money.json";

    private static Javalin server;

    @BeforeClass
    public static void setUp() {
        server = TestJavalinServer.createServer().start(PORT);
    }

    @AfterClass
    public static void tearDown() {
        server.stop();
    }

    @Before
    public void init() {
        ServerConfig.initializeDB();
    }

    @After
    public void cleanUp() {
        ServerConfig.cleanUpDB();
    }

    @Test
    public void test_givenTestAccounts_whenTransferMoney_ThenMoneyTransferredStatus() {
        given()
                .contentType(ContentType.JSON)
                .body(new File(REQUEST_TRANSFER_MONEY_JSON))
                .when()
                .post(URL + "/transfer")
                .then()
                .assertThat()
                .statusCode(200)
                .and()
                .body(equalTo(Status.TRANSFERRED.getStatus()));

    }
}
