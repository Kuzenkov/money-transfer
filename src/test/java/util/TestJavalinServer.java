package util;

import com.revolut.money.transfer.controller.AccountController;
import com.revolut.money.transfer.controller.MoneyTransferController;
import com.revolut.money.transfer.exception.ServiceException;
import com.revolut.money.transfer.repository.AccountRepository;
import com.revolut.money.transfer.service.AccountService;
import com.revolut.money.transfer.service.MoneyTransferService;
import io.javalin.Javalin;

import static io.javalin.apibuilder.ApiBuilder.crud;
import static io.javalin.apibuilder.ApiBuilder.post;

public class TestJavalinServer {

    private static final String DEFAULT_CONTENT_TYPE = "application/json";

    public static Javalin createServer() {
        final AccountRepository accountRepository = new AccountRepository();
        final AccountService accountService = new AccountService(accountRepository);
        final MoneyTransferService moneyTransferService = new MoneyTransferService(accountRepository);
        Javalin app = Javalin.create(conf -> conf.defaultContentType = DEFAULT_CONTENT_TYPE);

        app.get("/", ctx -> ctx.result("Hello World"));
        app.routes(() -> {
            crud("accounts/:iban", new AccountController(accountService));
            post("accounts/transfer", new MoneyTransferController(moneyTransferService));
        });
        app.exception(ServiceException.class, (e, ctx) -> {
            ctx.status(500);
            ctx.result(e.getMessage());
        });

        return app;
    }
}
