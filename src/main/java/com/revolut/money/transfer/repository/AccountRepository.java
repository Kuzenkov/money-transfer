package com.revolut.money.transfer.repository;

import com.revolut.money.transfer.repository.dao.Account;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;

import java.util.List;
import java.util.Optional;

@Slf4j
public class AccountRepository {

    public List<Account> findALL(Session session) {
        return session.createQuery("SELECT a FROM Account a", Account.class).getResultList();
    }

    public Optional<Account> findByIban(Session session, String iban) {
        return Optional.of(session.get(Account.class, iban));
    }

    public Account save(Session session, Account account) {
        session.save(account);
        return account;
    }

    public Account update(Session session, Account account) {
        session.update(account);
        return account;
    }

    public String delete(Session session, String iban) {
        final var account = session.get(Account.class, iban);
        session.delete(account);
        return iban;
    }

    public String delete(Session session, Account account) {
        session.delete(account);
        return account.getIban();
    }
}
