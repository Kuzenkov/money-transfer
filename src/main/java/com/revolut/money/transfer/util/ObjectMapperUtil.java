package com.revolut.money.transfer.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

@Slf4j
public final class ObjectMapperUtil {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private ObjectMapperUtil() {
    }

    public static ObjectMapper getObjectMapper() {
        return OBJECT_MAPPER;
    }

    public static <T> T readStringValue(String value, Class<T> clazz) {
        T result = null;
        try {
            result = OBJECT_MAPPER.readValue(value, clazz);
        } catch (IOException e) {
            log.error("Could not read Json object: {}", e.getMessage(), e);
        }
        return result;
    }

    public static <T> String writeValueAsString(T object) {
        String string = "";
        try {
            string = OBJECT_MAPPER.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            log.error("Could not write value as a String: {}", e.getMessage(), e);
        }
        return string;
    }
}
