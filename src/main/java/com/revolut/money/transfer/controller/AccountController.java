package com.revolut.money.transfer.controller;

import com.revolut.money.transfer.controller.dto.AccountDto;
import com.revolut.money.transfer.repository.dao.Account;
import com.revolut.money.transfer.service.AccountService;
import com.revolut.money.transfer.util.ObjectMapperUtil;
import io.javalin.apibuilder.CrudHandler;
import io.javalin.http.Context;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Optional;

public class AccountController implements CrudHandler {

    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public void create(@NotNull Context context) {
        final String body = context.body();
        final AccountDto accountDto = ObjectMapperUtil.readStringValue(body, AccountDto.class);
        final Account account = ObjectMapperUtil.getObjectMapper().convertValue(accountDto, Account.class);
        this.accountService.save(account);
        context.result(context.body());
        context.status(201);
    }

    @Override
    public void delete(@NotNull Context context, @NotNull String s) {
        this.accountService.delete(s);
        context.status(202);
    }

    @Override
    public void getAll(@NotNull Context context) {
        final List<Account> all = this.accountService.findAll();
        final String response = ObjectMapperUtil.writeValueAsString(all);
        context.status(200);
        context.result(response);
    }

    @Override
    public void getOne(@NotNull Context context, @NotNull String iban) {
        final Optional<Account> account = this.accountService.find(iban);
        if (account.isPresent()) {
            final String accountAsString = ObjectMapperUtil.writeValueAsString(account.get());
            context.result(accountAsString);
            context.status(200);
        } else {
            context.status(404);
            context.result(String.format("Account with iban: %s, not found.", iban));
        }
    }

    @Override
    public void update(@NotNull Context context, @NotNull String iban) {
        final AccountDto accountDto = ObjectMapperUtil.readStringValue(context.body(), AccountDto.class);
        final Account account = ObjectMapperUtil.getObjectMapper().convertValue(accountDto, Account.class);
        final Account updatedAccount = this.accountService.update(account);
        context.status(204);
        context.result(ObjectMapperUtil.writeValueAsString(updatedAccount));
    }
}
