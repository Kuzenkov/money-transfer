package com.revolut.money.transfer.controller;

import com.revolut.money.transfer.controller.dto.TransferMoneyDto;
import com.revolut.money.transfer.service.MoneyTransferService;
import com.revolut.money.transfer.service.Status;
import com.revolut.money.transfer.util.ObjectMapperUtil;
import io.javalin.http.Context;
import io.javalin.http.Handler;
import org.jetbrains.annotations.NotNull;

public class MoneyTransferController implements Handler {

    private final MoneyTransferService moneyTransferService;

    public MoneyTransferController(MoneyTransferService moneyTransferService) {
        this.moneyTransferService = moneyTransferService;
    }

    @Override
    public void handle(@NotNull Context ctx) throws Exception {
        final String body = ctx.body();
        final TransferMoneyDto transferMoneyDto = ObjectMapperUtil.readStringValue(body, TransferMoneyDto.class);
        moneyTransferService.transferMoney(transferMoneyDto);
        ctx.result(Status.TRANSFERRED.getStatus());
        ctx.status(200);
    }
}
