package com.revolut.money.transfer.controller.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class TransferMoneyDto {

    @NonNull
    @JsonProperty("fromIbanAccount")
    private String fromIban;

    @NonNull
    @JsonProperty("toIbanAccount")
    private String toIban;

    @NonNull
    @JsonProperty("transferAmount")
    private BigDecimal amount;
}
