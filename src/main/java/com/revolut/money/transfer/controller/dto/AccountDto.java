package com.revolut.money.transfer.controller.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountDto {

    @NonNull
    @JsonProperty("iban")
    private String iban;

    @NonNull
    @JsonProperty("firstName")
    private String firstName;

    @NonNull
    @JsonProperty("lastName")
    private String lastName;

    @NonNull
    @JsonProperty("amount")
    private BigDecimal amount;

    @NonNull
    @JsonProperty("phone")
    private String phone;

    @JsonProperty("email")
    private String email;
}
