package com.revolut.money.transfer.service;

import com.revolut.money.transfer.controller.dto.TransferMoneyDto;
import com.revolut.money.transfer.exception.ServiceException;
import com.revolut.money.transfer.repository.AccountRepository;
import com.revolut.money.transfer.util.HibernateUtil;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.Transaction;

@Slf4j
public class MoneyTransferService {

    private final AccountRepository accountRepository;

    public MoneyTransferService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public Status transferMoney(TransferMoneyDto transferMoney) {
        Status status = Status.TRANSFERRED;
        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();

            final var fromAccount = this.accountRepository.findByIban(session, transferMoney.getFromIban());
            final var toAccount = this.accountRepository.findByIban(session, transferMoney.getToIban());
            if (fromAccount.isEmpty() || toAccount.isEmpty()) {
                throw new ServiceException("Could not transfer money between accounts. One or both of given account(s) is/are invalid");
            }
            final var withdrawAccount = fromAccount.get();
            final var replenishAccount = toAccount.get();
            withdrawAccount.setAmount(withdrawAccount.getAmount().subtract(transferMoney.getAmount()));
            replenishAccount.setAmount(replenishAccount.getAmount().add(transferMoney.getAmount()));
            this.accountRepository.update(session, withdrawAccount);
            this.accountRepository.update(session, replenishAccount);

            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            log.error("Could not transfer money from Account: {} to Account: {} -> {}",
                    transferMoney.getFromIban(),
                    transferMoney.getToIban(),
                    e.getMessage(), e);
            throw new ServiceException("Could not transfer money between accounts.", e);
        }

        return status;
    }
}
