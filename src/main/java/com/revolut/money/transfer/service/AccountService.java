package com.revolut.money.transfer.service;

import com.revolut.money.transfer.exception.ServiceException;
import com.revolut.money.transfer.repository.AccountRepository;
import com.revolut.money.transfer.repository.dao.Account;
import com.revolut.money.transfer.util.HibernateUtil;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;
import java.util.Optional;

@Slf4j
public class AccountService {

    private final AccountRepository accountRepository;

    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public List<Account> findAll() {

        List<Account> accounts;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            accounts = this.accountRepository.findALL(session);
        } catch (Exception e) {
            log.error("Could not find Accounts {}", e.getMessage(), e);
            throw new ServiceException("Could not find Accounts", e);
        }

        return accounts;
    }

    public Optional<Account> find(String iban) {

        Optional<Account> account = Optional.empty();

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            account = this.accountRepository.findByIban(session, iban);
            if (account.isEmpty()) {
                throw new Exception(String.format("Account not found, iban: %s", iban));
            }
        } catch (Exception e) {
            log.error("Could not find Account: {} -> {}", iban, e.getMessage(), e);
            throw new ServiceException(String.format("Could not find Account: %s -> %s", iban, e.getMessage()), e);
        }

        return account;
    }

    public Account save(Account account) {
        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            this.accountRepository.save(session, account);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            log.error("Could not create Account: {} -> {}", account.getIban(), e.getMessage(), e);
            throw new ServiceException(String.format("Could not create Account: %s -> %s", account.getIban(), e.getMessage()), e);
        }

        return account;
    }

    public Account update(Account account) {
        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            this.accountRepository.update(session, account);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            log.error("Could not update Account: {} -> {}", account.getIban(), e.getMessage(), e);
            throw new ServiceException(String.format("Could not update Account: %s -> %s", account.getIban(), e.getMessage()), e);
        }

        return account;
    }

    public String delete(String iban) {
        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            final var account = this.accountRepository.findByIban(session, iban);
            final var acc = account.orElseThrow(() -> new ServiceException(String.format("Account not found, IBAN: %s", iban)));
            this.accountRepository.delete(session, acc);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            log.error("Could not delete Account: {} -> {}", iban, e.getMessage(), e);
            throw new ServiceException(String.format("Could not delete Account: %s -> %s", iban, e.getMessage()), e);
        }
        return iban;
    }

    public String delete(Account account) {
        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.delete(account);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            log.error("Could not delete Account: {} -> {}", account.getIban(), e.getMessage(), e);
            throw new ServiceException(String.format("Could not delete Account: %s -> %s", account.getIban(), e.getMessage()), e);
        }
        return account.getIban();
    }
}
