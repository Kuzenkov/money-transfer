package com.revolut.money.transfer.service;

public enum Status {
    TRANSFERRED("TRANSFERRED"),
    PENDING("PENDING"),
    ACCEPTED("ACCEPTED");

    private String status;

    private Status(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
