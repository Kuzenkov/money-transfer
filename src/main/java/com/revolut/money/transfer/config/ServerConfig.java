package com.revolut.money.transfer.config;

import com.revolut.money.transfer.repository.AccountRepository;
import com.revolut.money.transfer.repository.dao.Account;
import com.revolut.money.transfer.service.AccountService;
import io.javalin.Javalin;
import lombok.extern.slf4j.Slf4j;
import org.yaml.snakeyaml.Yaml;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;

@Slf4j
public final class ServerConfig {

    private static final String PROPERTIES = "src/main/resources/properties.yml";

    public static Javalin configJavalinServer(JavalinCustomConfig config) {
        return Javalin.create(conf -> {
            conf.defaultContentType = config.getDefaultContentType();
        });
    }

    public static Configuration config() {
        Configuration config = new Configuration();
        final Yaml yaml = new Yaml();
        try (InputStream in = Files.newInputStream(Paths.get(PROPERTIES))) {
            config = yaml.loadAs(in, Configuration.class);
        } catch (IOException e) {
            log.error("Could not parse property file: {}", e.getMessage(), e);
        }
        return config;
    }

    public static void initializeDB() {
        final AccountService accountService = new AccountService(new AccountRepository());
        accountService.save(new Account("DE89370400440532013000", "Kerstin", "Goldschmidt", new BigDecimal("1578.34"),
                "0490421868174", "KerstinGoldschmidt@dayrep.com"));
        accountService.save(new Account("SE3550000000054910000003", "Yasmin", "Berggren", new BigDecimal("2345.23"),
                "460465515333", "YasminBerggren@armyspy.com"));
        accountService.save(new Account("PL27114020040000300201355387", "Tadeusz", "Adamski", new BigDecimal("1249.53"),
                "48728533882", "TadeuszAdamski@dayrep.com"));
        accountService.save(new Account("UA9300762011623852957", "Terence", "Tokaryev", new BigDecimal("9045.29"),
                "37724252487", "TerenceTokaryev@jourrapide.com"));
    }

    public static void cleanUpDB() {
        final AccountService accountService = new AccountService(new AccountRepository());
        accountService.findAll()
                .forEach(accountService::delete);
    }
}
