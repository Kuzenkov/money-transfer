package com.revolut.money.transfer.config;

import lombok.Data;

@Data
public class JavalinCustomConfig {

    private String defaultContentType;

    private Integer port;
}
