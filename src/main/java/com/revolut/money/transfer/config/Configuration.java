package com.revolut.money.transfer.config;

import lombok.Data;

@Data
public class Configuration {

    private JavalinCustomConfig javalinConfig;
}
