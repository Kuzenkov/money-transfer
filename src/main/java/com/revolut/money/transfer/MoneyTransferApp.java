package com.revolut.money.transfer;

import com.revolut.money.transfer.config.Configuration;
import com.revolut.money.transfer.config.ServerConfig;
import com.revolut.money.transfer.controller.AccountController;
import com.revolut.money.transfer.controller.MoneyTransferController;
import com.revolut.money.transfer.controller.dto.TransferMoneyDto;
import com.revolut.money.transfer.exception.ServiceException;
import com.revolut.money.transfer.repository.AccountRepository;
import com.revolut.money.transfer.service.AccountService;
import com.revolut.money.transfer.service.MoneyTransferService;
import com.revolut.money.transfer.util.ObjectMapperUtil;
import io.javalin.Javalin;
import io.javalin.plugin.json.JavalinJackson;

import java.util.Objects;

import static io.javalin.apibuilder.ApiBuilder.crud;
import static io.javalin.apibuilder.ApiBuilder.post;

public class MoneyTransferApp {

    public static void main(String[] args) {
        final AccountRepository accountRepository = new AccountRepository();
        final AccountService accountService = new AccountService(accountRepository);
        final MoneyTransferService moneyTransferService = new MoneyTransferService(accountRepository);
        Javalin app = null;
        final Configuration config = ServerConfig.config();

        JavalinJackson.configure(ObjectMapperUtil.getObjectMapper());
        app = ServerConfig
                .configJavalinServer(config.getJavalinConfig())
                .events(eventListener -> {
                    eventListener.serverStarted(ServerConfig::initializeDB);
                })
                .start(config.getJavalinConfig().getPort());
        app.routes(() -> {
            crud("accounts/:iban", new AccountController(accountService));
            post("accounts/transfer", new MoneyTransferController(moneyTransferService));
        });
        app.before("accounts/transfer", ctx -> ctx.bodyValidator(TransferMoneyDto.class)
                .check(dto -> ((dto.getAmount().longValue() > 0)
                                && Objects.nonNull(dto.getFromIban())
                                && !"".equals(dto.getFromIban().trim())
                                && Objects.nonNull(dto.getToIban())
                                && !"".equals(dto.getToIban().trim())),
                        String.format("The request is not valid: %s", ctx.body())));
        app.exception(ServiceException.class, (e, ctx) -> {
            ctx.status(500);
            ctx.result(e.getMessage());
        });
    }
}
