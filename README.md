# MONEY TRANSFER API

The REST API to transfer money between accounts.

## Prerequisites

JDK/JRE version 11.* 

maven build tool

## How to run

`mvn clean package`

`java -jar target/MoneyTransferService-0.1-SNAPSHOT.jar` 

The application is started on port `8080`

## Endpoints
The API is accessible on host `localhost` port `8080`.
This API has next endpoint:
- `GET /accounts` - return list of created accounts
- `POST /accounts` - create new account
- `GET /accounts/{ibna}` - get specific account
- `PATCH /accounts/{iban}` - update specific account
- `DELETE /accounts/{iban}` - delete specific account
- `POST /accounts/transfer` - transfer money between accounts

### Request payload examples
All request examples can be found in the following directory `/src/main/resources/scratches/api_calls.http`
- Create new account: URL - `POST http://localhost:8080/accounts`
```
{
  "iban": "3",
  "firstName": "User3",
  "lastName": "User3",
  "amount": 1500.57,
  "phone": "123456789",
  "email": "user3@email.com"
}
```

- Update account: URL - `PATCH http://localhost:8080/accounts/DE89370400440532013000`
```
{
  "iban": "DE89370400440532013000",
  "firstName": "User3",
  "lastName": "User3",
  "amount": 1500.57,
  "phone": "123456789",
  "email": "user3@email.com"
}
```
- Delete account: URL - `DELETE http://localhost:8080/accounts/DE89370400440532013000`

- Get all accounts: URL - `GET http://localhost:8080/accounts`

- Get specific account: URL - `GET http://localhost:8080/accounts/DE89370400440532013000`

- Transfer money between accounts: URL - `POST http://localhost:8080/accounts/transfer`
```
{
  "fromIbanAccount": "DE89370400440532013000",
  "toIbanAccount": "PL27114020040000300201355387",
  "transferAmount": "1000.00"
}
```

## License

This project is released under [LICENSE.md](LICENSE.md)
